var gpu = component.get("gpu");
var screen = component.get("screen");

component.invoke(gpu, "bind", screen);
var res = component.invoke(gpu, "getResolution");

component.invoke(gpu, "fill", 1, 1, res[0], res[1], " ");
component.invoke(gpu, "set", 1, 1, "This is coming from an EEPROM BIOS!");
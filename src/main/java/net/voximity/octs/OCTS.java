package net.voximity.octs;

import com.google.common.io.ByteStreams;
import li.cil.oc.api.Items;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import java.io.IOException;

@Mod(modid = OCTS.MODID, version = OCTS.VERSION)
public class OCTS {
    public static final String MODID = "octs";
    public static final String VERSION = "0.1.0";

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        li.cil.oc.api.Machine.add(RhinoArch.class);

        try {
            byte[] code = ByteStreams.toByteArray(OCTS.class.getClassLoader().getResourceAsStream("bios.js"));
            Items.registerEEPROM("EEPROM (JS BIOS)", code, new byte[0], false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Finished initializing. Architectures added!");
    }
}

package net.voximity.octs;

import li.cil.oc.api.machine.Architecture;
import li.cil.oc.api.machine.ExecutionResult;
import li.cil.oc.api.machine.Machine;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

@Architecture.Name("Rhino")
public class RhinoArch implements Architecture {
    private Machine machine;
    private RhinoAPI vm;

    public RhinoArch(Machine machine) {
        this.machine = machine;
    }

    @Override
    public boolean isInitialized() {
        if (vm == null) return false;
        return vm.initialized;
    }

    @Override
    public boolean initialize() {
        vm = new RhinoAPI(machine, false);
        return true;
    }

    @Override
    public void close() {
        vm = null;
    }

    @Override
    public void runSynchronized() {
        vm.runSync();
    }

    @Override
    public ExecutionResult runThreaded(boolean isSynchronizedReturn) {
        return vm.runThreaded(isSynchronizedReturn);
    }

    @Override public boolean recomputeMemory(Iterable<ItemStack> arg) {
        return true;
    }

    @Override public void onConnect() {}
    @Override public void load(NBTTagCompound nbt) {}
    @Override public void save(NBTTagCompound nbt) {}
    @Override public void onSignal() {}
}

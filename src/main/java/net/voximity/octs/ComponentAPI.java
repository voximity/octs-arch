package net.voximity.octs;

import li.cil.oc.api.machine.LimitReachedException;
import li.cil.oc.api.machine.Machine;

import java.util.Map;

public class ComponentAPI {
    private Machine machine;
    private RhinoAPI vm;

    public ComponentAPI(Machine machine, RhinoAPI vm) {
        this.machine = machine;
        this.vm = vm;
    }

    public Map<String, String> list() {
        return machine.components();
    }

    public String get(String componentName) {
        String address = "";
        for (Map.Entry<String, String> component: machine.components().entrySet()) {
            if (component.getValue().equals(componentName)) {
                address = component.getKey();
                break;
            }
        }
        return address.isEmpty() ? null : address;
    }

    public Object[] invoke(String address, String method, Object... params) {
        try {
            return machine.invoke(address, method, params);
        } catch (LimitReachedException e) {
            vm.addInvoke(new InvokeCallback(machine, address, method, params));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Object[0];
    }

    public void print(Object object) {
        System.out.println(object.toString());
    }
}

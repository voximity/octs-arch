package net.voximity.octs;

import li.cil.oc.api.machine.LimitReachedException;
import li.cil.oc.api.machine.Machine;

public class InvokeCallback {
    private String address;
    private String method;
    private Object[] params;
    private Machine machine;

    InvokeCallback(Machine machine, String address, String method, Object[] params) {
        this.machine = machine;
        this.method = method;
        this.address = address;
        this.params = params;
    }

    public void call(RhinoAPI vm) {
        try {
            Object[] results = machine.invoke(address, method, params);
        } catch(LimitReachedException e) {
            vm.addInvoke(this);
        } catch(Exception e) {
            // ?
        }
    }
}

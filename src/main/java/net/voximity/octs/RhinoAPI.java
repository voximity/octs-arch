package net.voximity.octs;

import li.cil.oc.api.machine.ExecutionResult;
import li.cil.oc.api.machine.LimitReachedException;
import li.cil.oc.api.machine.Machine;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

enum State {
    SHUTDOWN,
    SLEEP,
    INVOKE
}

public class RhinoAPI {
    Machine machine;
    ComponentAPI component;

    ScriptableObject scope;

    State state;
    boolean reboot = false;
    int sleepTime = 0;
    List<InvokeCallback> invokeCallbacks = new ArrayList<InvokeCallback>();

    public boolean initialized = false;

    public RhinoAPI(Machine machine, boolean iDontNeedThis) {
        this.machine = machine;

        Context context = Context.enter();
        scope = context.initStandardObjects();
        component = new ComponentAPI(machine, this);

        ScriptableObject.putProperty(scope, "component", Context.javaToJS(component, scope));
    }

    public void runSync() {}

    public void addInvoke(InvokeCallback callback) {
        if (state == State.SLEEP) {
            invokeCallbacks.add(callback);
            state = State.INVOKE;
        }
    }

    public void shutdown(boolean reboot) {
        state = State.SHUTDOWN;
        this.reboot = reboot;
    }

    public ExecutionResult runThreaded(boolean syncReturn) {
        state = State.SLEEP;
        Context context = Context.enter();
        context.initStandardObjects(scope);

        if (!initialized) {
            String eeprom = component.get("eeprom");

            if (eeprom == null) return new ExecutionResult.Error("No EEPROM");

            String bios = "";

            try {
                byte[] biosIn = (byte[])machine.invoke(eeprom, "get", new Object[0])[0];
                bios = new String(biosIn);

                context.evaluateString(scope, bios, "bios", 1, null);
            } catch(LimitReachedException e) {
                return new ExecutionResult.Error("Invoke limit reached.");
            } catch(ScriptException e) {
                return new ExecutionResult.Error(e.getMessage());
            } catch (Exception e) {
                return new ExecutionResult.Error(e.getMessage());
            }

            initialized = true;
        } else {
            if (invokeCallbacks.size() > 0) {
                int size = Math.min(invokeCallbacks.size(), 10);
                for (int i = 0; i < size; i++) {
                    invokeCallbacks.get(i).call(this);
                }

                invokeCallbacks.subList(0, size).clear();
            }

            // onSignal thing
        }

        switch (state) {
            case SHUTDOWN: return new ExecutionResult.Shutdown(reboot);
            case SLEEP: return new ExecutionResult.Sleep(sleepTime);
            case INVOKE: return new ExecutionResult.Sleep(0);
            default: return new ExecutionResult.Sleep(sleepTime);
        }
    }
}
